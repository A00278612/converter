public class ConvertBetweenCurrencies {

        // Partial Product Skeleton Code

        double convertCurrency (double exchangeRate, double amountUSD ) throws ConvertBetweenCurreniesExceptionHandler {

            // This method converts US dollar values to Euro values based on the
            // current exchange rate between U.S. dollars and and the Euro.
            // On successful conversion, the Euro amount is returned.

            // Additional Requirement - Define your own exception handler to
            // handle any invalid values.

            if (amountUSD <= 0) throw new ConvertBetweenCurreniesExceptionHandler("usd amount less then 0");
            return amountUSD * exchangeRate;


        }



}
