import junit.framework.TestCase;

public class ConvertBetweenCurrenciesTest extends TestCase {
    // This method converts US dollar values to Euro values based on the
    // current exchange rate between U.S. dollars and and the Euro.
    // On successful conversion, the Euro amount is returned.
    public void testConvertCurrency001() {
        // test number : 001
        // objective: verify at that $1 can converted
        // Test type: junit
        // input: 1
        // output: .84

        ConvertBetweenCurrencies cur = new ConvertBetweenCurrencies();
        try {
            assertEquals(.84, cur.convertCurrency(.84, 1));
        } catch (Exception e) {
            fail();
        }
    }

    public void testConvertCurrency002() {
        // test number : 002
        // objective: verify at that $20 can converted
        // Test type: junit
        // input: 20
        // output: 16.8

        ConvertBetweenCurrencies cur = new ConvertBetweenCurrencies();
        try {
            assertEquals(16.8, cur.convertCurrency(.84, 20));
        } catch (Exception e) {
            fail();
        }
    }

    public void testConvertCurrency003() {
        // test number : 003
        // objective: verify at that $0 can not be converted
        // Test type: junit
        // input: 0
        // output: usd amount less then 0

        ConvertBetweenCurrencies cur = new ConvertBetweenCurrencies();
        try {
            cur.convertCurrency(.84, 0);
        } catch (ConvertBetweenCurreniesExceptionHandler e) {
            assertEquals("usd amount less then 0", e.getMessage());
        } catch (Exception e){
            fail();
        }

    }

    public void testConvertCurrency004() {
        // test number : 003
        // objective: verify at that $-1 can not be converted
        // Test type: junit
        // input: -1
        // output: usd amount less then 0

        ConvertBetweenCurrencies cur = new ConvertBetweenCurrencies();
        try {
            cur.convertCurrency(.84, -1);
        } catch (ConvertBetweenCurreniesExceptionHandler e) {
            assertEquals("usd amount less then 0", e.getMessage());
        } catch (Exception e){
            fail();
        }

    }

    public void testConvertCurrency005() {
        // test number : 003
        // objective: verify at that $.10 can be converted
        // Test type: junit
        // input: 0.10
        // output: 0.084

        ConvertBetweenCurrencies cur = new ConvertBetweenCurrencies();
        try {
            assertEquals(0.084, cur.convertCurrency(.84, 0.10));
        } catch (Exception e){
            fail();
        }

    }
}